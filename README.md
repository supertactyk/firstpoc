# Arbreole - First Proof Of Concept

- This is the first POC of the Arbreole architecture.
- It connects 3 LEDS to an Arduino to a Raspberry Pi to a Server and to a Client interface.
- Link to the tutorial video: https://www.youtube.com/watch?v=M2-nXbi3qmk

## Description of the result of the POC
### 1. Client interface _(see server/signal.php)_
![Client interface](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2018.54.57.png)

### 2. The submitted command is received by the `php web server` _(see server/welcome.php)_ and transfers the data to the Raspberry Pi via `python socket` _(see server/server.py)_
![Server sends data to the Raspberry Pi](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2019.02.36.png)

### 3. The Raspberry Pi receives the data and sends data to the Arduino via `Serial protocol` _(see raspberryPi/lec.py)_
![Submit the command](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2019.22.54.png)

### 4. The Arduino turns on the right LED _(see arduino/3LEDs.ino)_
![Arduino turns the LED on](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2019.27.54.png)

### 5. The data is acknowledged and goes the other way till the client interface
![Client interface receives back the acknowledged data](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2019.31.04.png)

## Installation

1. [Setup Hardware and Arduino](Arduino/setup_hardware_arduino.md)
2. [Setup Raspberry Pi](RaspberryPi/setup_raspberry_pi.md)
3. [Setup Server](Server/setup_server.md)
4. [Instructions of use](modus_operandi.md)
