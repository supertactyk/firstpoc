# First POC - Raspberry Pi part

## Prerequisites

- [Get started with Raspberry Pi](https://www.imore.com/how-get-started-using-raspberry-pi)
- It is recommended to be able to access to your Raspberry Pi in headless mode (without the need of a screen, directly via ssh)
- You will need to execute some `python` scripts, `python` environment should be already installed in your RPi.
- You need to be on the same network than your Raspberry Pi, either the same Internet network or local network.

## Identifying the Arduino device ID

- All the devices attached to the RPi are shown running the command:
- `sudo ls /dev/`
- Disconnect the Arduino from the RPi and run:
- `sudo ls /dev/ >>dev.txt` this will save the list of the devices in a dev.txt file.
- Connect the Arduino to the RPi and run:
- `sudo ls /dev/ >>dev1.txt` this will save the list of the devices in a dev1.txt file.
- Get the name of the Arduino device by making the diff between those .txt files:
- `diff dev.txt dev1.txt`
- The device name should look like `ttyACM0`

## Test the RPi / Arduino serial connection

- From your RPi terminal, launch python
- `python`
  - `import serial`
  - `ser=serial.Serial('/dev/ttyACM0', 9600)` starts the Serial communication between RPi and the Arduino device (ttyACM0, see last section)
  - `ser.write('1')`
- **The first LED connected to the Arduino should turns on!**
- _NB: You can stop the python with the keyboard shortcut `ctrl + D`_

## Prepare the python script

- Create a file named `lec.py` (or a name of your choice) in your RPi in a choosen folder
- Copy / paste the code from the [lec.py](lec.py) file in it
- Set the const `TCP_IP` with your Raspberry Pi IP address. (Get this IP address running `ifconfig` in the Raspberry Pi terminal)
- To run the script, just run:
- `python lec.py`
- NB: running this script starts the Serial communication between RPi and the Arduino. It then waits for instructions comming from the server. 

# Your Raspberry Pi is now all set!

- [Previous: setup your hardware and Arduino](../Arduino/setup_hardware_arduino.md)
- [Next: setup your Server](../Server/setup_server.md)
