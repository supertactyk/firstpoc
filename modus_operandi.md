
# Modus Operandi

## Prerequisites
- Make sure you have followed the 3 first steps

 1. [Setup Hardware and Arduino](Arduino/setup_hardware_arduino.md)
 2. [Setup Raspberry Pi](RaspberryPi/setup_raspberry_pi.md)
 3. [Setup Server](Server/setup_server.md)

- Make sure that the 3 LEDs are correctly connected, that the Arduino is plugged to the Raspberry Pi, that the Raspberry Pi is powered and that the php environment (XAMPP) is started.


## How to use this first POC

1. Start the Raspberry Pi python script: `python lec.py`
2. In your local machine, connect to [http://localhost:8080/firstPOC/signal.php](http://localhost:8080/firstPOC/signal.php)
3. Voilà ! You can now send commands via this interface

- `"1"` will make LED1 (pin 13) blinks
- `"2"` will make LED2 (pin 9) blinks
- `"3"` will make LED3 (pin 8) blinks
- `"7"` will stop the connection

- To change this behaviour, you can modify the files [RaspberryPi/lec.py](RaspberryPi/lec.py) along with the file [Arduino/3LEDs.ino](Arduino/3LEDs.ino)
