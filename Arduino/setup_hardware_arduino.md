# First POC - Hardware & Arduino part

## 1. Hardware

- For the first POC, the electronic schematic is very basic, we are connecting 3 LEDs to the Arduino board (pin 8, 9, 13)
- This is the schematic of how to connect one LED.
- ![Hardware Led Schema](https://bitbucket.org/supertactyk/firstpoc/downloads/hardware_schematic_small.png)
- The purpose of resistor R is to regulate the current that circulates in the LED
- You can [calculate the LED resistor value by applying Ohm's law](http://www.ohmslawcalculator.com/led-resistor-calculator)
- Do the same for the other 2 LEDs and you obtain the final schematic
- ![Hardware Led Picture](https://bitbucket.org/supertactyk/firstpoc/downloads/hardware_pic_small.png)

## 2. Upload the code to the Arduino

- Install the [Arduino Software (IDE)](https://www.arduino.cc/en/Guide/HomePage) accordingly to your operating system
- Plug your Arduino to your computer via the USB cable
- Set the correct Board and the correct Port
 - ![Set the Arduino Board](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2019.58.55.png)
 - ![Set the Arduino Port](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2020.00.40.png)
- Create a file named `3LEDs.ino` (or a name of your choice) in the Arduino Sketch IDE.
- Copy / paste the code from the [3LEDs.ino](3LEDs.ino) file
- Verify / Compile (cmd + R)
- Upload (cmd + U)
- _NB: the Arduino UNO's pin 13 is reset when the Arduino boots and when a serial connection starts._

### I want to see some LED blinking!

- As of now, you might be frustrated and want to see at least one LED blinks to make sure you are properly set.
- You are strongly encouraged to familiarize yourself with the .ino code.
- For example you can put the following part in the `void setup()` function and upload your Arduino code again. After the upload, the 1st LED will turn on for a duration of 1 second.
```
  digitalWrite(ledPin1, HIGH);
  delay(1000);
  digitalWrite(ledPin1, LOW);
  delay(1000);
```    

# Your Hardware / Arduino is now all set!

- [Previous: README.md](../README.md)
- [Next: setup your Raspberry Pi](../RaspberryPi/setup_raspberry_pi.md)
