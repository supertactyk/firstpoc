# First POC - Server part

## Prerequisites

- You will need a Php development environment. 
- You can use [XAMPP](https://www.apachefriends.org/fr/index.html) for example. 
- If you have a Mac, it is recommended to follow the Getting started of [XAMPP-VM for Mac](https://www.apachefriends.org/blog/new_xampp_20170628.html)
- The rest of this doc is written for XAMPP-VM for Mac. It might need a few changes in order to run the server on a Linux environment

## Setup the server environment XAMPP

- Start the XAMPP server in the `XAMPP > General` tab. Once started, an IP address is attributed to your Virtual Machine (for example 192.168.64.2)
- Enable the portforwarding in the `XAMPP > Network` tab. That way you will be able to access to the VM via the url http://localhost:8080/ 
- Install python3.6 
  - Click on the `Terminal` button in the `XAMPP > General` tab to open the XAMPP-VM Terminal (or direct ssh to the VM using the command `ssh root@192.168.64.2` where `192.168.64.2` is your XAMPP-VM ip address)
  - run:
  - `sudo apt-get update`
  - then run:
  - `sudo apt-get install python3.6`
  - test that python3 is correctly installed by running `python3 --version`

## Upload your files to the server

- Mount the `/opt/lampp` volume in the `XAMPP > Volumes` tab. Clicking on the `Explore` button connects to the XAMPP virtual machine and opens its `/opt/lampp` directory in a finder window.
- Create a folder named `firstPOC` (or a name of your choice) in the XAMPP-VM `lampp/htdocs` directory
- For example, if you have created a folder named `firstPOC`, it will be available via the url [http://localhost:8080/firstPOC/](http://localhost:8080/firstPOC/) on your local machine (or [http://yourVMIPAddress/firstPOC](http://yourVMIPAddress/firstPOC) if you don't use port forwarding)
- Put the php files `signal.php` and `welcome.php` in this folder
- Put the python file `server.py` in this folder.
- In the `server.py` file, set the const `TCP_IP` with your Raspberry Pi's IP address. (Get this IP address running `ifconfig` in the Raspberry Pi terminal)

## Access to the server

- The url to access to the server is: [http://localhost:8080/firstPOC/signal.php](http://localhost:8080/firstPOC/signal.php) or [http://yourVMIPAddress/firstPOC/signal.php](http://yourVMIPAddress/firstPOC/signal.php)
- You should see an interface with a simple form used to submit a command
- ![Client interface](https://bitbucket.org/supertactyk/firstpoc/downloads/Screen%20Shot%202017-11-29%20at%2018.54.57.png)

# Your [server](http://localhost:8080/firstPOC/signal.php) is now all set!

- [Previous: setup your Raspberry Pi](../RaspberryPi/setup_raspberry_pi.md)
- [Next: Instructions of use](../modus_operandi.md)


