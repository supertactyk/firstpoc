#!/usr/bin/env python3

import socket
import sys


TCP_IP = '192.168.1.10' # this IP address should be the same as the one in RaspberryPi/lec.py
TCP_PORT = 5005 # this TCP port should be the same as the one in RaspberryPi/lec.py
BUFFER_SIZE = 1024 # this buffer size should be the same as the one in RaspberryPi/lec.py
MESSAGE = bytes(sys.argv[1], 'UTF-8')

SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
SOCKET.connect((TCP_IP, TCP_PORT))
SOCKET.send(MESSAGE)
DATA = SOCKET.recv(BUFFER_SIZE)
SOCKET.close()

print("received data:", DATA.decode())
